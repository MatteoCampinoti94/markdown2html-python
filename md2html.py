import sys
import re


def convert(ifile, ofile, prg=True):
    bold = False
    ital = False
    strk = False
    code = False
    pref = False
    parg = False

    for l in range(0, len(ifile)):
        line = ifile[l]
        head = 0

        # Heading
        if prg and not parg and re.match(r'^(\#+) .+$', line):
            head = len(re.match(r'^(\#+) .+$', line).group(1))
            if head < 7:
                ofile.write(f'<h{head}>')
            else:
                head = 0

        # Horizontal bar
        elif prg and not parg and line.strip() == '---':
            ofile.write('<hr/>\n')
            continue

        if prg and not parg and line and not head:
            ofile.write('<p>')
            parg = True

        i = head

        while i < len(line):
            # Bold and Italic
            if not code + pref and line[i] in ('*', '_'):
                if line[i:i + 2] in ('**', '__'):
                    ofile.write(f'<{"/"*bold}b>')
                    bold = not bold
                    i += 1
                else:
                    ofile.write(f'<{"/"*ital}i>')
                    ital = not ital

            # Strikethrough
            elif not code + pref and line[i:i + 2] == '~~':
                ofile.write(f'<{"/"*strk}del>')
                strk = not strk
                i += 1

            # Code & preformatted text
            elif line[i] == '`':
                if prg and line[i:i + 3] == '```' and not head:
                    ofile.write(f'<{"/"*pref}pre>')
                    pref = not pref
                    i += 2
                elif not pref:
                    ofile.write(f'<{"/"*code}code>')
                    code = not code
                else:
                    ofile.write(line[i])

            # Link
            elif line[i] == '[':
                if re.match(r'^\[[^\]]*\]( *)\([^\)]+\).*$', line[i:]):
                    tag = re.match(
                        r'^(\[([^\]]*)\]( *)\(([^\)]+)\)).*$', line[i:]).groups()
                    txt = tag[1]
                    url = tag[3].strip()

                    url = re.match(r'^([^"]+)[ ]*("[^"]*"|)$', url).groups()
                    tit = url[1].strip('"')
                    url = url[0].strip()

                    ofile.write(f'<a href="{url}" title="{tit}">')
                    convert([txt], ofile, False)
                    ofile.write('</a>')
                    i += len(tag[0]) - 1
                else:
                    ofile.write(line[i])

            # Image
            elif line[i] == '!':
                if re.match(r'^!\[[^\]]*\]( *)\([^\)]+\).*$', line[i:]):
                    tag = re.match(
                        r'^(!\[([^\]]*)\]( *)\(([^\)]+)\)).*$', line[i:]).groups()
                    alt = tag[1]
                    url = tag[3].strip()

                    url = re.match(r'^([^"]+)[ ]*("[^"]*"|)$', url).groups()
                    tit = url[1].strip('"')
                    url = url[0].strip()

                    ofile.write(
                        f'<img src="{url}" alt="{alt}" title="{tit}"/>')
                    i += len(tag[0]) - 1
                else:
                    ofile.write(line[i])

            # Normal characters
            else:
                ofile.write(line[i])

            i += 1

        if head:
            ofile.write(f'</h{head}>')

        if prg and parg and (not ifile[l + 1:l + 2] or not ifile[l + 1]):
            ofile.write('</p>')
            parg = False

        ofile.write('\n' if prg else '')


def main(ifile):
    if type(ifile) != str:
        raise TypeError

    ofile = re.sub(r'\.(md|markdown)$', '', ifile) + '.html'

    try:
        ifile = [line.strip() for line in open(ifile, 'r').readlines()]
    except FileNotFoundError:
        raise

    ofile = open(ofile, 'w')

    convert(ifile, ofile)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit(1)

    main(sys.argv[1])
